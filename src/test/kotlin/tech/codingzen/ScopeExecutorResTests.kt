package tech.codingzen

import kotlinx.coroutines.runBlocking
import org.junit.Test
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isEqualTo
import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.ok
import tech.codingzen.kata.result.suspendRes
import tech.codingzen.kdi.KdiException
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.data_structure.Kdi.Companion.moduleSpec
import tech.codingzen.kdi.logging.Logger

class ScopeExecutorResTests {
  @Test
  fun `test error`() {
    val errMsg = "uh oh"
    expectThrows<KdiException> {
      runBlocking {
        Kdi.appSpec()
          .printLogger(Logger.Level.TRACE)
          .noScopeExtensions()
          .spec(moduleSpec("foo") {})
          .execute(ScopeExecutorRes { scopeDsl ->
            suspendRes {
              err(errMsg)
            }
          })
      }
    }.and { get { this.message }.isEqualTo(errMsg) }
  }

  @Test
  fun `test error inline ScopeExecutorRes`() {
    val errMsg = "uh oh"
    expectThrows<KdiException> {
      runBlocking {
        Kdi.appSpec()
          .printLogger(Logger.Level.TRACE)
          .noScopeExtensions()
          .spec(moduleSpec("foo") {})
          .executeRes { err(errMsg) }
      }
    }.and { get { this.message }.isEqualTo(errMsg) }
  }

  @Test
  fun `test success`() {
    val result = runBlocking {
      Kdi.appSpec()
        .printLogger(Logger.Level.TRACE)
        .noScopeExtensions()
        .spec(moduleSpec("foo") {})
        .execute(ScopeExecutorRes { Res.ok(1) })
    }
    expectThat(result).isEqualTo(1)
  }

  @Test
  fun `test success inline ScopeExecutorRes`() {
    val result = runBlocking {
      Kdi.appSpec()
        .printLogger(Logger.Level.TRACE)
        .noScopeExtensions()
        .spec(moduleSpec("foo") {})
        .executeRes { 1 }
    }
    expectThat(result).isEqualTo(1)
  }
}