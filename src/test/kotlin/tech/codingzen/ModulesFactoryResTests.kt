package tech.codingzen

import kotlinx.coroutines.runBlocking
import org.junit.Test
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isEqualTo
import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.ok
import tech.codingzen.kata.result.res
import tech.codingzen.kata.result.suspendRes
import tech.codingzen.kdi.KdiException
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.data_structure.Kdi.Companion.moduleSpec
import tech.codingzen.kdi.data_structure.KdiModule
import tech.codingzen.kdi.dsl.module
import tech.codingzen.kdi.dsl.modules
import tech.codingzen.kdi.logging.Logger

class ModulesFactoryResTests {
  @Test
  fun `test error`() {
    val errMsg = "uh oh"
    val spec = Kdi.scopeSpec("foo")
      .bootstrapper {
        ModulesFactoryRes {
          suspendRes {
            err(errMsg)
          }
        }
      }
      .build()

    expectThrows<KdiException> {
      runBlocking {
        Kdi.appSpec()
          .printLogger(Logger.Level.TRACE)
          .noScopeExtensions()
          .spec(spec)
          .executeRes { "should fail in bootstrap" }
      }
    }.and { get { this.cause!!.message }.isEqualTo(errMsg) }
  }

  @Test
  fun `test success`() {
    val spec = Kdi.scopeSpec("foo")
      .bootstrapper {
        ModulesFactoryRes {
          suspendRes {
            modules {
              +module {
                instance(12)
              }
            }
          }
        }
      }
      .build()

    val result =
      runBlocking {
        Kdi.appSpec()
          .printLogger(Logger.Level.TRACE)
          .noScopeExtensions()
          .spec(spec)
          .executeRes { it.get<Int>() }
      }
    expectThat(result).isEqualTo(12)
  }

}