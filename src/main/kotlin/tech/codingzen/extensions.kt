package tech.codingzen

import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.SuspendResDsl
import tech.codingzen.kata.result.suspendRes
import tech.codingzen.kdi.dsl.ScopeDsl
import tech.codingzen.kdi.dsl.builder.kdi_app.KdiAppSpecBuilders

suspend fun <T> KdiAppSpecBuilders.FinalStep.executeRes(block: suspend SuspendResDsl.(ScopeDsl) -> T): T {
  val executor = object: ScopeExecutorRes<T> {
    override suspend fun executeRes(scopeDsl: ScopeDsl): Res<T> = suspendRes { block(scopeDsl) }
  }
  return this.execute(executor)
}