package tech.codingzen

import tech.codingzen.kata.result.Err
import tech.codingzen.kata.result.Ok
import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.parts
import tech.codingzen.kdi.KdiException
import tech.codingzen.kdi.data_structure.KdiModule
import tech.codingzen.kdi.data_structure.KdiModulesFactory

fun interface ModulesFactoryRes : KdiModulesFactory {
  override suspend fun create(): List<KdiModule> =
    when (val outcome = createRes()) {
      is Ok -> outcome.value
      is Err -> outcome.parts.let { (ctx, exc) ->
        val message = ctx.joinToString(System.lineSeparator())
        throw if (exc != null) KdiException(message, exc) else KdiException(message)
      }
    }

  suspend fun createRes(): Res<List<KdiModule>>
}