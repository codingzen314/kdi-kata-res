package tech.codingzen

import tech.codingzen.kata.result.Err
import tech.codingzen.kata.result.Ok
import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.parts
import tech.codingzen.kdi.KdiException
import tech.codingzen.kdi.data_structure.ScopeExecutor
import tech.codingzen.kdi.dsl.ScopeDsl

fun interface ScopeExecutorRes<out T> : ScopeExecutor<T> {
  override suspend fun execute(scopeDsl: ScopeDsl): T =
    when (val outcome = executeRes(scopeDsl)) {
      is Ok -> outcome.value
      is Err -> outcome.parts.let { (ctx, exc) ->
        val message = ctx.joinToString(System.lineSeparator())
        throw if (exc != null) KdiException(message, exc) else KdiException(message)
      }
    }

  suspend fun executeRes(scopeDsl: ScopeDsl): Res<T>
}
